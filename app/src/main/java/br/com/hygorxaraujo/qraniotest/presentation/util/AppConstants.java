package br.com.hygorxaraujo.qraniotest.presentation.util;

/**
 * Created by hygor on 12/14/16.
 **/
public class AppConstants {

    public static final String PARCEL_KEY_USER = "parcel_key_user";
    public static final String PARCEL_KEY_USERNAME = "parcel_key_username";
    public static final String PARCEL_KEY_PASSWORD = "parcel_key_userpassword";
    public static final String PARCEL_KEY_APOD = "parcel_key_apod";
    public static final String PARCEL_KEY_PATENT_LIST = "parcel_key_patent_list";
}
