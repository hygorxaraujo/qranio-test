package br.com.hygorxaraujo.qraniotest.presentation.apod;

import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/18/16.
 **/
public interface ApodView
        extends MvpView {

    void setApodData(Apod apod);

    void showDatePicker();

    void showProgress(int msgResId);

    void hideProgress();

    void showMessage(int msgResId);
}
