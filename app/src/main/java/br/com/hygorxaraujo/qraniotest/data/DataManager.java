package br.com.hygorxaraujo.qraniotest.data;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.BuildConfig;
import br.com.hygorxaraujo.qraniotest.data.local.PreferencesHelper;
import br.com.hygorxaraujo.qraniotest.data.local.UserDAO;
import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.data.remote.ApodService;
import br.com.hygorxaraujo.qraniotest.data.remote.PatentService;
import rx.Observable;

/**
 * Created by hygor on 12/13/16.
 **/
public class DataManager {

    public static final String TAG = DataManager.class.getSimpleName();
    private UserDAO userDAO;
    private PreferencesHelper prefsHelper;
    private ApodService apodService;
    private PatentService patentService;

    @Inject
    public DataManager(UserDAO userDAO,
                       PreferencesHelper preferencesHelper,
                       ApodService apodService,
                       PatentService patentService) {
        this.userDAO = userDAO;
        this.prefsHelper = preferencesHelper;
        this.apodService = apodService;
        this.patentService = patentService;
    }

    public User authUser(String username, String password) {
        User user = userDAO.authUser(username, password);
        if (user != null) {
            putUserData(user);
        }
        return user;
    }

    public long createUser(String firstname, String lastname, String username, String email, String password, String phone) {
        return userDAO.createUser(firstname, lastname, username, email, password, phone);
    }

    private void putUserData(User user) {
        prefsHelper.putUserData(user);
    }

    public User getLoggedInUser() {
        return prefsHelper.getLoggedInUser();
    }

    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    public Observable<Apod> getTodaysApod() {
        return apodService.getApod(BuildConfig.API_KEY);
    }

    public void logoutUser() {
        prefsHelper.clear();
    }

    public Observable<Apod> getApodByDate(int year, int month, int day) {
        StringBuilder date = new StringBuilder();
        date.append(year);
        date.append("-");
        date.append(month + 1);
        date.append("-");
        date.append(day);
        return apodService.getApodByDate(BuildConfig.API_KEY, date.toString());
    }

    public Observable<PatentList> getPatents(String query) {
        return patentService.getPatents(BuildConfig.API_KEY, 15, query);
    }
}
