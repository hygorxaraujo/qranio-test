package br.com.hygorxaraujo.qraniotest.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.injection.ApplicationContext;

/**
 * Created by hygor on 12/14/16.
 **/

public class PreferencesHelper {

    private static final String PREF_FILE_NAME = "qranio_test_app_pref_file";
    private static final String KEY_FIRSTNAME = "key_firstname";
    private static final String KEY_LASTNAME = "key_lastname";
    private static final String KEY_USERNAME = "key_username";
    private static final String KEY_EMAIL = "key_email";
    private static final String KEY_PHONE = "key_phone";

    private final SharedPreferences preferences;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        this.preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        preferences.edit().clear().apply();
    }

    public void putUserData(User user) {
        preferences.edit()
                .putString(KEY_FIRSTNAME, user.getFirstName())
                .putString(KEY_LASTNAME, user.getLastName())
                .putString(KEY_USERNAME, user.getUserName())
                .putString(KEY_EMAIL, user.getEmail())
                .putString(KEY_PHONE, user.getPhone())
                .apply();
    }

    public User getLoggedInUser() {
        User user = new User();
        user.setFirstName(preferences.getString(KEY_FIRSTNAME, null));
        user.setLastName(preferences.getString(KEY_LASTNAME, null));
        user.setUserName(preferences.getString(KEY_USERNAME, null));
        user.setEmail(preferences.getString(KEY_EMAIL, null));
        user.setPhone(preferences.getString(KEY_PHONE, null));
        if (user.getFirstName() == null ||
                user.getLastName() == null ||
                user.getUserName() == null) {
            return null;
        }
        return user;
    }
}
