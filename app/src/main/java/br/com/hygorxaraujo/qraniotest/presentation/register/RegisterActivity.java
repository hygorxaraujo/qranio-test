package br.com.hygorxaraujo.qraniotest.presentation.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.presentation.base.BaseMvpActivity;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Mask;
import br.com.hygorxaraujo.qraniotest.presentation.util.Messenger;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class RegisterActivity
        extends BaseMvpActivity<RegisterView, RegisterPresenter>
        implements RegisterView, Validator.ValidationListener {

    @BindView(R.id.toolbar_register)
    Toolbar toolbar;

    @BindView(R.id.register_til_firstname)
    @NotEmpty(trim = true, messageResId = R.string.validation_msg_field_not_empty)
    @Length(trim = true, min = 3, messageResId = R.string.validation_msg_min_3_chars)
    TextInputLayout tilFirstName;

    @BindView(R.id.register_til_lastname)
    @NotEmpty(trim = true, messageResId = R.string.validation_msg_field_not_empty)
    @Length(trim = true, min = 3, messageResId = R.string.validation_msg_min_3_chars)
    TextInputLayout tilLastName;

    @BindView(R.id.register_til_username)
    @NotEmpty(trim = true, messageResId = R.string.validation_msg_field_not_empty)
    @Length(trim = true, min = 3, messageResId = R.string.validation_msg_min_3_chars)
    TextInputLayout tilUserName;

    @BindView(R.id.register_til_email)
    @NotEmpty(trim = true, messageResId = R.string.validation_msg_field_not_empty)
    @Email(messageResId = R.string.validation_msg_invalid_email)
    TextInputLayout tilEmail;

    @BindView(R.id.register_til_password)
    @NotEmpty(trim = true, messageResId = R.string.validation_msg_field_not_empty)
    @Password(messageResId = R.string.validation_msg_invalid_password)
    @Length(trim = true, min = 3, messageResId = R.string.validation_msg_min_3_chars)
    TextInputLayout tilPassword;

    @BindView(R.id.register_til_phone)
    TextInputLayout tilPhone;

    @Inject
    RegisterPresenter registerPresenter;

    @Inject
    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.register_toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tilPhone.getEditText()
                .addTextChangedListener(Mask.insert("(##)#####-####",
                        tilPhone.getEditText()));
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public RegisterPresenter createPresenter() {
        return registerPresenter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //region OnClick
    @OnClick(R.id.register_btn_finalize)
    public void onFinalizeClick() {
        validator.validate();
    }
    //endregion

    //region Validation
    @Override
    public void onValidationSucceeded() {
        registerPresenter.onValidationSucceeded(tilFirstName.getEditText().getText().toString(),
                tilLastName.getEditText().getText().toString(),
                tilUserName.getEditText().getText().toString(),
                tilEmail.getEditText().getText().toString(),
                tilPassword.getEditText().getText().toString(),
                tilPhone.getEditText().getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(message);
                ((TextInputLayout) view).setErrorEnabled(true);
            } else {
                Messenger.showShort(this, message);
            }
        }
    }


    @OnTextChanged(value = {R.id.register_tiet_firstname, R.id.register_tiet_lastname,
            R.id.register_tiet_username, R.id.register_tiet_password},
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onAfterTextChanged() {
        tilFirstName.setError(null);
        tilFirstName.setErrorEnabled(false);
        tilLastName.setError(null);
        tilLastName.setErrorEnabled(false);
        tilUserName.setError(null);
        tilUserName.setErrorEnabled(false);
        tilEmail.setError(null);
        tilEmail.setErrorEnabled(false);
        tilPassword.setError(null);
        tilPassword.setErrorEnabled(false);
    }
    //endregion

    //region View
    @Override
    public void showMessage(int msgResId) {
        Messenger.showShort(this, msgResId);
    }

    @Override
    public void navigateBackToLogin(String username, String password) {
        Intent intent = new Intent();
        intent.putExtra(AppConstants.PARCEL_KEY_USERNAME, username);
        intent.putExtra(AppConstants.PARCEL_KEY_PASSWORD, password);
        setResult(RESULT_OK, intent);
        finish();
    }
    //endregion
}
