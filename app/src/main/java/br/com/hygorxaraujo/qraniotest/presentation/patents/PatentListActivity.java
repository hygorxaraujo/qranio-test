package br.com.hygorxaraujo.qraniotest.presentation.patents;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.Patent;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.presentation.base.BaseMvpActivity;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Messenger;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PatentListActivity
        extends BaseMvpActivity<PatentListView, PatentListPresenter>
        implements PatentListView, SwipeRefreshLayout.OnRefreshListener,
        PatentListAdapter.PatentsItemClickListener, SearchView.OnQueryTextListener,
        MenuItemCompat.OnActionExpandListener {

    @BindView(R.id.patentlist_cl)
    CoordinatorLayout clPatents;

    @BindView(R.id.toolbar_patent)
    Toolbar toolbar;

    @BindView(R.id.patent_rv_content)
    RecyclerView rvContent;

    @BindView(R.id.patent_srl_content)
    SwipeRefreshLayout srlContent;

    @BindView(R.id.patent_tv_message)
    TextView tvMessage;

    @BindView(R.id.patent_pb_loading)
    ProgressBar pbLoading;

    @Inject
    protected PatentListPresenter patentPresenter;

    @Inject
    protected PatentListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patent_list);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.patent_toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setUpSwipeRefresh();
        PatentList patentList = Parcels.unwrap(getIntent().getParcelableExtra(AppConstants.PARCEL_KEY_PATENT_LIST));
        patentPresenter.onDataAvailable(patentList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        patentPresenter.onViewResumed();
    }

    private void setUpSwipeRefresh() {
        srlContent.setOnRefreshListener(this);
        srlContent.setColorSchemeResources(R.color.colorAccent);
        srlContent.setProgressBackgroundColorSchemeResource(R.color.colorPrimary);
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public PatentListPresenter createPresenter() {
        return patentPresenter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_patent, menu);

        final MenuItem menuSearch = menu.findItem(R.id.patent_search);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(menuSearch);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.patent_search_title));
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(menuSearch, this);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        patentPresenter.onRefresh();
    }

    //region View
    @Override
    public void setUpPatentList(List<Patent> patents) {
        rvContent.setLayoutManager(new LinearLayoutManager(this));
        rvContent.setAdapter(adapter);
        adapter.setPatents(patents);
        adapter.setListener(this);
    }

    @Override
    public void showProgress() {
        pbLoading.setVisibility(View.VISIBLE);
        rvContent.setVisibility(View.GONE);
        tvMessage.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        pbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(int msgResId) {
        tvMessage.setText(msgResId);
        tvMessage.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvContent.setVisibility(View.GONE);
    }

    @Override
    public void updatePatentList(List<Patent> patents) {
        adapter.setPatents(patents);
        if (srlContent.isRefreshing()) {
            srlContent.setRefreshing(false);
        }
    }

    @Override
    public void showContent() {
        rvContent.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        tvMessage.setVisibility(View.GONE);
    }

    @Override
    public void showPatentAbstract(String patentAbstract) {
        Messenger.showSnackbar(clPatents, patentAbstract);
    }

    //endregion

    @Override
    public void onPatentItemClick(Patent patent) {
        patentPresenter.onPatentItemClick(patent);
    }

    //region MenuItem Expand/Collapse
    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return true;
    }
    //endregion

    //region QueryTextListener
    @Override
    public boolean onQueryTextSubmit(String query) {
        patentPresenter.onQueryTextChange(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        patentPresenter.onQueryTextChange(newText);
        return false;
    }
    //endregion
}
