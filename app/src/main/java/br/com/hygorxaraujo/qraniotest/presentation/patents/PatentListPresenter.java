package br.com.hygorxaraujo.qraniotest.presentation.patents;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.DataManager;
import br.com.hygorxaraujo.qraniotest.data.model.Patent;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;
import br.com.hygorxaraujo.qraniotest.presentation.util.Logger;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hygor on 12/20/16.
 **/
public class PatentListPresenter
        extends MvpPresenter<PatentListView> {

    private static final String TAG = PatentListPresenter.class.getSimpleName();
    private PatentList patentList;
    private DataManager dataManager;
    private Subscription subscription;

    @Inject
    public PatentListPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    protected void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = null;
    }

    @Override
    public void onDettach() {
        super.onDettach();
        unsubscribe();
    }

    public void onDataAvailable(PatentList patentList) {
        this.patentList = patentList;
    }

    public void onViewResumed() {
        if (isViewAttached()) {
            getView().setUpPatentList(patentList.getResults());
        }
    }

    public void onRefresh() {
        if (isViewAttached()) {
            unsubscribe();
            getPatents(null);
        }
    }

    public void onPatentItemClick(Patent patent) {
        if (isViewAttached()) {
            getView().showPatentAbstract(patent.getPatentAbstract());
        }
    }

    public void onQueryTextChange(String query) {
        if (isViewAttached()) {
            unsubscribe();
            if (query.length() > 3) {
                getView().showProgress();
                getPatents(query);
            } else if (query.isEmpty()) {
                getView().showProgress();
                getPatents(null);
            }
        }
    }

    private void getPatents(String query) {
        subscription = dataManager.getPatents(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PatentList>() {
                    @Override
                    public void onCompleted() {
                        unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(TAG, e.getMessage());
                        if (isViewAttached()) {
                            getView().hideProgress();
                            getView().showMessage(R.string.patentlist_msg_error_patents);
                        }
                        unsubscribe();
                    }

                    @Override
                    public void onNext(PatentList patentList) {
                        if (isViewAttached()) {
                            getView().hideProgress();
                            if (patentList.getResults() != null
                                    && !patentList.getResults().isEmpty()) {
                                getView().updatePatentList(patentList.getResults());
                                getView().showContent();
                            } else {
                                getView().showMessage(R.string.patentlist_msg_empty_patents);
                            }
                        }
                    }
                });
    }
}
