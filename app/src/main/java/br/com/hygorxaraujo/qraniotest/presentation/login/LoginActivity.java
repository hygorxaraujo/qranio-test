package br.com.hygorxaraujo.qraniotest.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.presentation.base.BaseMvpActivity;
import br.com.hygorxaraujo.qraniotest.presentation.register.RegisterActivity;
import br.com.hygorxaraujo.qraniotest.presentation.users.UserListActivity;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Messenger;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity
        extends BaseMvpActivity<LoginView, LoginPresenter>
        implements LoginView, Validator.ValidationListener {

    public static final int REGISTER_REQUEST_CODE = 201;

    @BindView(R.id.login_til_username)
    @NotEmpty(messageResId = R.string.validation_msg_field_not_empty)
    TextInputLayout tilUsername;

    @BindView(R.id.login_til_password)
    @Password(messageResId = R.string.validation_msg_invalid_password)
    @NotEmpty(messageResId = R.string.validation_msg_field_not_empty)
    TextInputLayout tilPassword;

    @BindView(R.id.login_toolbar)
    Toolbar toolbar;

    @Inject
    protected Validator validator;

    @Inject
    protected LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginPresenter.onViewResume();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public LoginPresenter createPresenter() {
        return loginPresenter;
    }

    //region OnClick
    @OnClick(R.id.login_btn_login)
    public void onLoginClick() {
        validator.validate();
    }

    @OnClick(R.id.login_btn_register)
    public void onRegisterClick() {
        loginPresenter.onRegisterClick();
    }
    //endregion

    //region Validation
    @Override
    public void onValidationSucceeded() {
        presenter.onValidationSucceeded(tilUsername.getEditText().getText().toString(),
                tilPassword.getEditText().getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof TextInputLayout) {
                ((TextInputLayout) view).setError(message);
                ((TextInputLayout) view).setErrorEnabled(true);
            } else {
                Messenger.showShort(this, message);
            }
        }
    }

    @OnTextChanged(value = {R.id.login_tiet_username, R.id.login_tiet_password},
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onAfterTextChanged() {
        tilUsername.setError(null);
        tilUsername.setErrorEnabled(false);
        tilPassword.setError(null);
        tilPassword.setErrorEnabled(false);
    }
    //endregion

    //region View
    @Override
    public void navigateToUserList(User user) {
        Intent intent = new Intent(this, UserListActivity.class);
        intent.putExtra(AppConstants.PARCEL_KEY_USER, Parcels.wrap(user));
        startActivity(intent);
        finish();
    }

    @Override
    public void showMessage(int msgResId) {
        Messenger.showShort(this, msgResId);
    }

    @Override
    public void navigateToRegisterNewUser() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, REGISTER_REQUEST_CODE);
    }
    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String username = data.getStringExtra(AppConstants.PARCEL_KEY_USERNAME);
                String password = data.getStringExtra(AppConstants.PARCEL_KEY_PASSWORD);
                tilUsername.getEditText().setText(username);
                tilPassword.getEditText().setText(password);
            }
        }
    }
}
