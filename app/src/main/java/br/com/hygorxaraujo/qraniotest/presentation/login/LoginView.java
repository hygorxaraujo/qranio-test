package br.com.hygorxaraujo.qraniotest.presentation.login;

import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/12/16.
 **/
public interface LoginView
        extends MvpView {

    void navigateToUserList(User user);

    void showMessage(int msgResId);

    void navigateToRegisterNewUser();
}
