package br.com.hygorxaraujo.qraniotest.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.data.local.UserContract.UserEntry;
import br.com.hygorxaraujo.qraniotest.injection.ActivityContext;

/**
 * Created by hygor on 12/13/16.
 **/
public class QranioTestDbHelper
        extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "QranioTest.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                    UserEntry.COLUMN_NAME_FIRSTNAME + " TEXT NOT NULL, " +
                    UserEntry.COLUMN_NAME_LASTNAME + " TEXT NOT NULL, " +
                    UserEntry.COLUMN_NAME_USERNAME + " TEXT NOT NULL UNIQUE, " +
                    UserEntry.COLUMN_NAME_EMAIL + " TEXT NOT NULL, " +
                    UserEntry.COLUMN_NAME_PASSWORD + " TEXT NOT NULL, " +
                    UserEntry.COLUMN_NAME_PHONE + " TEXT" + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;

    @Inject
    public QranioTestDbHelper(@ActivityContext Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL(SQL_DELETE_ENTRIES);
        onCreate(database);
    }
}
