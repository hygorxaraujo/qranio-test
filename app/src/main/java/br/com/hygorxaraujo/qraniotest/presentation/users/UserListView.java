package br.com.hygorxaraujo.qraniotest.presentation.users;

import java.util.List;

import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/13/16.
 **/
public interface UserListView
        extends MvpView {

    void setUpUserList(List<User> users);

    void updateUserList(List<User> users);

    void navigateToRegisterNewUser();

    void setUpNavView(User user);

    void showUserData(User user);

    void showMessage(int msgResId);

    void navigateToApod(Apod apod);

    void showProgress(int msgResId);

    void hideProgress();

    void navigateToLogin();

    void navigateToPatentList(PatentList patentList);
}
