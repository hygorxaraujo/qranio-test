package br.com.hygorxaraujo.qraniotest.data.remote;

import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by hygor on 12/18/16.
 **/
public interface ApodService {

    @GET("planetary/apod")
    Observable<Apod> getApod(@Query("api_key") String apiKey);

    @GET("planetary/apod")
    Observable<Apod> getApodByDate(@Query("api_key") String apiKey,
                                   @Query("date") String date);
}
