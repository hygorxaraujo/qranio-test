package br.com.hygorxaraujo.qraniotest.presentation.users;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hygor on 12/14/16.
 **/
public class UserListAdapter
        extends RecyclerView.Adapter<UserListAdapter.UsersViewHolder> {

    private List<User> users;

    private UsersItemClickListener listener;

    public interface UsersItemClickListener {
        void onListUserClick(User user);
    }

    @Inject
    public UserListAdapter() {
        users = new ArrayList<>();
    }

    @Override
    public UsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.userlist_item_user, parent, false);
        return new UsersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UsersViewHolder holder, int position) {
        User user = users.get(position);
        holder.setUser(user);
        holder.tvUsername.setText(user.getUserName());
        holder.tvName.setText(user.getFirstName() + " " + user.getLastName());
    }

    @Override
    public int getItemCount() {
        return users != null ? users.size() : 0;
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    public void setListener(UsersItemClickListener listener) {
        this.listener = listener;
    }

    //region UsersViewHolder
    protected class UsersViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.useritem_tv_username)
        TextView tvUsername;

        @BindView(R.id.useritem_tv_name)
        TextView tvName;

        private User user;

        public UsersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.useritem_rl)
        public void onUserItemClick() {
            listener.onListUserClick(user);
        }

        public void setUser(User user) {
            this.user = user;
        }
    }
    //endregion
}
