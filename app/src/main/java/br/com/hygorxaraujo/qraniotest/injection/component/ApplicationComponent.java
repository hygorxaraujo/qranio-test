package br.com.hygorxaraujo.qraniotest.injection.component;

import android.content.Context;

import javax.inject.Singleton;

import br.com.hygorxaraujo.qraniotest.injection.ApplicationContext;
import br.com.hygorxaraujo.qraniotest.injection.module.ApplicationModule;
import dagger.Component;

/**
 * Created by hygor on 12/13/16.
 **/
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();
}
