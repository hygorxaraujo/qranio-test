package br.com.hygorxaraujo.qraniotest.data.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.List;

/**
 * Created by hygor on 12/20/16.
 **/
@Parcel(Parcel.Serialization.BEAN)
public class Patent {

    private String category;
    @SerializedName("reference_number")
    private String referenceNumber;
    @SerializedName("abstract")
    private String patentAbstract;
    private String title;
    private List<Innovator> innovator;

    @ParcelConstructor
    public Patent() {
    }

    //region Getters&Setters
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getPatentAbstract() {
        return patentAbstract;
    }

    public void setPatentAbstract(String patentAbstract) {
        this.patentAbstract = patentAbstract;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Innovator> getInnovator() {
        return innovator;
    }

    public void setInnovator(List<Innovator> innovator) {
        this.innovator = innovator;
    }
    //endregion
}
