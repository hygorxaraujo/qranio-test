package br.com.hygorxaraujo.qraniotest.presentation.adapter;

import android.support.design.widget.TextInputLayout;

import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.exception.ConversionException;

/**
 * Created by hygor on 12/12/16.
 **/
public class TextInputLayoutViewDataAdapter
        implements ViewDataAdapter<TextInputLayout, String> {

    @Override
    public String getData(TextInputLayout view) throws ConversionException {
        return view.getEditText() != null ? view.getEditText().getText().toString() : null;
    }
}
