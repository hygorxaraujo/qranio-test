package br.com.hygorxaraujo.qraniotest.presentation.users;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.DataManager;
import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;
import br.com.hygorxaraujo.qraniotest.presentation.util.Logger;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hygor on 12/13/16.
 **/
public class UserListPresenter
        extends MvpPresenter<UserListView> {

    public static final String TAG = UserListPresenter.class.getSimpleName();

    private DataManager dataManager;
    private User user;
    private Subscription subscription;

    @Inject
    public UserListPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    protected void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = null;
    }

    @Override
    public void onDettach() {
        super.onDettach();
        unsubscribe();
    }

    public void onViewResumed(User user) {
        if (isViewAttached()) {
            this.user = user;
            List<User> users = dataManager.getUsers();
            getView().setUpUserList(users);
            getView().setUpNavView(user);
        }
    }

    public void onRefresh() {
        if (isViewAttached()) {
            updateUserList();
        }
    }

    private void updateUserList() {
        if (isViewAttached()) {
            List<User> users = dataManager.getUsers();
            getView().updateUserList(users);
        }
    }

    public void onAddUserClick() {
        if (isViewAttached()) {
            getView().navigateToRegisterNewUser();
        }
    }

    public void onNewUserRegistered() {
        if (isViewAttached()) {
            updateUserList();
        }
    }

    public void onUserDataClick() {
        if (isViewAttached()) {
            getView().showUserData(user);
        }
    }

    public void onApodClick() {
        if (isViewAttached()) {
            getView().showProgress(R.string.userlist_msg_loading_todays_apod);
            unsubscribe();
            subscription = dataManager.getTodaysApod()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Apod>() {
                        @Override
                        public void onCompleted() {
                            Logger.d(TAG, "onCompleted");
                            if (isViewAttached()) {
                                getView().hideProgress();
                            }
                            unsubscribe();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(TAG, e.getMessage());
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().showMessage(R.string.userlist_msg_error_apod);
                            }
                            unsubscribe();
                        }

                        @Override
                        public void onNext(Apod apod) {
                            Logger.d(TAG, "onNext");
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().navigateToApod(apod);
                            }
                        }
                    });
        }
    }

    public void onLogoutClick() {
        if (isViewAttached()) {
            dataManager.logoutUser();
            getView().navigateToLogin();
        }
    }

    public void onListUserClick(User user) {
        if (isViewAttached()) {
            getView().showUserData(user);
        }
    }

    public void onPatentsClick() {
        if (isViewAttached()) {
            getView().showProgress(R.string.userlist_msg_loading_patents);
            unsubscribe();
            subscription = dataManager.getPatents(null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<PatentList>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(TAG, e.getMessage());
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().showMessage(R.string.userlist_msg_error_patents);
                            }
                            unsubscribe();
                        }

                        @Override
                        public void onNext(PatentList patentList) {
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().navigateToPatentList(patentList);
                            }
                        }
                    });
        }
    }
}
