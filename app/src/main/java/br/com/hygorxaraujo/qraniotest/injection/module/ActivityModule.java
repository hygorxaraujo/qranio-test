package br.com.hygorxaraujo.qraniotest.injection.module;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;

import com.mobsandgeeks.saripaar.Validator;

import br.com.hygorxaraujo.qraniotest.injection.ActivityContext;
import br.com.hygorxaraujo.qraniotest.presentation.adapter.TextInputLayoutViewDataAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by hygor on 12/13/16.
 **/
@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return activity;
    }

    @Provides
    Validator provideValidator(@ActivityContext Context context) {
        Validator validator = new Validator(context);
        validator.registerAdapter(TextInputLayout.class, new TextInputLayoutViewDataAdapter());
        validator.setValidationListener((Validator.ValidationListener) context);
        return validator;
    }
}
