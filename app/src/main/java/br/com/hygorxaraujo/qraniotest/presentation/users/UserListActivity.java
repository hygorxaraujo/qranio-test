package br.com.hygorxaraujo.qraniotest.presentation.users;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.presentation.apod.ApodActivity;
import br.com.hygorxaraujo.qraniotest.presentation.base.BaseMvpActivity;
import br.com.hygorxaraujo.qraniotest.presentation.login.LoginActivity;
import br.com.hygorxaraujo.qraniotest.presentation.patents.PatentListActivity;
import br.com.hygorxaraujo.qraniotest.presentation.register.RegisterActivity;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Messenger;
import br.com.hygorxaraujo.qraniotest.presentation.util.ProgressDialogHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static br.com.hygorxaraujo.qraniotest.presentation.login.LoginActivity.REGISTER_REQUEST_CODE;

/**
 * Created by hygor on 12/13/16.
 **/
public class UserListActivity
        extends BaseMvpActivity<UserListView, UserListPresenter>
        implements UserListView, NavigationView.OnNavigationItemSelectedListener,
        SwipeRefreshLayout.OnRefreshListener, UserListAdapter.UsersItemClickListener {

    @BindView(R.id.userlist_toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.userlist_rv_content)
    RecyclerView rvContent;

    @BindView(R.id.userlist_srl_content)
    SwipeRefreshLayout srlContent;

    @BindView(R.id.userlist_fab_add_user)
    FloatingActionButton fabAddUser;

    @Inject
    protected UserListAdapter adapter;

    @Inject
    protected UserListPresenter userListPresenter;

    @Inject
    protected ProgressDialogHelper progressDialogHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setUpSwipeRefresh();
    }

    private void setUpSwipeRefresh() {
        srlContent.setOnRefreshListener(this);
        srlContent.setColorSchemeResources(R.color.colorAccent);
        srlContent.setProgressBackgroundColorSchemeResource(R.color.colorPrimary);
    }

    @Override
    protected void onResume() {
        super.onResume();
        userListPresenter.onViewResumed((User) Parcels.unwrap(getIntent()
                .getParcelableExtra(AppConstants.PARCEL_KEY_USER)));
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public UserListPresenter createPresenter() {
        return userListPresenter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_userlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.userlist_action_logout:
                userListPresenter.onLogoutClick();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //region Navigation View
    @Override
    public void setUpNavView(User user) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.userlist_open_drawer, R.string.userlist_close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView username = (TextView) headerView.findViewById(R.id.navheader_tv_username);
        username.setText(user.getUserName());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navview_menuitem_user_data:
                userListPresenter.onUserDataClick();
                break;
            case R.id.navview_menuitem_nasa_apod:
                userListPresenter.onApodClick();
                break;
            case R.id.navview_menuitem_nasa_patent:
                userListPresenter.onPatentsClick();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void navigateToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //endregion

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {
        userListPresenter.onRefresh();
    }

    //region View
    @Override
    public void setUpUserList(List<User> users) {
        rvContent.setLayoutManager(new LinearLayoutManager(this));
        rvContent.setAdapter(adapter);
        adapter.setUsers(users);
        adapter.setListener(this);
    }

    @Override
    public void updateUserList(List<User> users) {
        adapter.setUsers(users);
        srlContent.setRefreshing(false);
    }

    @Override
    public void navigateToRegisterNewUser() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, REGISTER_REQUEST_CODE);
    }

    @Override
    public void showUserData(User user) {
        DialogFragment dialogFragment = UserDataDialogFragment.newInstance(user);
        dialogFragment.show(getSupportFragmentManager(), "user_data_dialog");
    }

    @Override
    public void showMessage(int msgResId) {
        Messenger.showShort(this, msgResId);
    }

    @Override
    public void navigateToApod(Apod apod) {
        Intent intent = new Intent(this, ApodActivity.class);
        intent.putExtra(AppConstants.PARCEL_KEY_APOD, Parcels.wrap(apod));
        startActivity(intent);
    }

    @Override
    public void showProgress(int msgResId) {
        progressDialogHelper.show(msgResId);
    }

    @Override
    public void hideProgress() {
        progressDialogHelper.dismiss();
    }

    @Override
    public void navigateToPatentList(PatentList patentList) {
        Intent intent = new Intent(this, PatentListActivity.class);
        intent.putExtra(AppConstants.PARCEL_KEY_PATENT_LIST, Parcels.wrap(patentList));
        startActivity(intent);
    }

    //endregion

    @Override
    public void onListUserClick(User user) {
        userListPresenter.onListUserClick(user);
    }

    @OnClick(R.id.userlist_fab_add_user)
    public void onAddUserClick() {
        userListPresenter.onAddUserClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                userListPresenter.onNewUserRegistered();
            }
        }
    }
}
