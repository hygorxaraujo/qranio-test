package br.com.hygorxaraujo.qraniotest.presentation.register;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.DataManager;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;
import br.com.hygorxaraujo.qraniotest.presentation.util.Mask;

/**
 * Created by hygor on 12/14/16.
 **/
public class RegisterPresenter
        extends MvpPresenter<RegisterView> {

    private DataManager dataManager;

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void onValidationSucceeded(String firstname, String lastname, String username, String email, String password, String phone) {
        if (isViewAttached()) {
            long newId = dataManager.createUser(firstname, lastname, username, email, password, Mask.unmask(phone));
            if (newId != -1) {
                getView().navigateBackToLogin(username, password);
            } else {
                getView().showMessage(R.string.register_msg_error_creating_user);
            }
        }
    }
}
