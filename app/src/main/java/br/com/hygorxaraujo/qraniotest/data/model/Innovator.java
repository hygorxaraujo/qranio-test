package br.com.hygorxaraujo.qraniotest.data.model;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

/**
 * Created by hygor on 12/20/16.
 **/
@Parcel(Parcel.Serialization.BEAN)
public class Innovator {

    private String fname;
    private String mname;
    private String lname;

    @ParcelConstructor
    public Innovator() {
    }

    //region Getters&Setters
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
    //endregion
}
