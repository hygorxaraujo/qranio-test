package br.com.hygorxaraujo.qraniotest.presentation.mvp;

/**
 * Created by hygor on 12/12/16.
 **/
public abstract class MvpPresenter<V extends MvpView> {

    protected V view;

    public boolean isViewAttached() {
        return view != null;
    }

    public void onAttach(V view) {
        this.view = view;
    }

    public void onDettach() {
        this.view = null;
    }

    public V getView() {
        return view;
    }
}
