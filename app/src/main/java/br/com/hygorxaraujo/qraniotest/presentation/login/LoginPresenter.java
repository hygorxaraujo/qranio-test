package br.com.hygorxaraujo.qraniotest.presentation.login;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.DataManager;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;

/**
 * Created by hygor on 12/12/16.
 **/
public class LoginPresenter
        extends MvpPresenter<LoginView> {

    private DataManager dataManager;

    @Inject
    public LoginPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void onViewResume() {
        User user = dataManager.getLoggedInUser();
        if (isViewAttached() && user != null) {
            getView().navigateToUserList(user);
        }
    }

    public void onValidationSucceeded(String username, String password) {
        if (isViewAttached()) {
            User user = dataManager.authUser(username, password);
            if (user != null) {
                getView().showMessage(R.string.login_msg_welcome);
                getView().navigateToUserList(user);
            } else {
                getView().showMessage(R.string.login_msg_user_or_pass_incorrect);
            }
        }
    }

    public void onRegisterClick() {
        if (isViewAttached()) {
            getView().navigateToRegisterNewUser();
        }
    }
}
