package br.com.hygorxaraujo.qraniotest.presentation.util;

import android.app.ProgressDialog;
import android.content.Context;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.injection.ActivityContext;

/**
 * Created by hygor on 12/18/16.
 **/
public class ProgressDialogHelper {

    private Context context;
    private ProgressDialog progressDialog;

    @Inject
    public ProgressDialogHelper(@ActivityContext Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(this.context);
    }

    public void show() {
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void show(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void show(int msgResId) {
        progressDialog.setMessage(context.getString(msgResId));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void show(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void show(int titleResId, int msgResId) {
        progressDialog.setTitle(titleResId);
        progressDialog.setMessage(context.getString(msgResId));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void cancel() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    public void setCancelable(boolean isCancelable) {
        progressDialog.setCancelable(isCancelable);
    }
}
