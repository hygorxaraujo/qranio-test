package br.com.hygorxaraujo.qraniotest.injection.module;

import br.com.hygorxaraujo.qraniotest.BuildConfig;
import br.com.hygorxaraujo.qraniotest.data.remote.ApodService;
import br.com.hygorxaraujo.qraniotest.data.remote.PatentService;
import br.com.hygorxaraujo.qraniotest.injection.ActivityScope;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hygor on 12/18/16.
 **/
@Module
public class WebModule {

    @Provides
    @ActivityScope
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    @ActivityScope
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @ActivityScope
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @ActivityScope
    ApodService provideApodService(Retrofit retrofit) {
        return retrofit.create(ApodService.class);
    }

    @Provides
    @ActivityScope
    PatentService providePatentService(Retrofit retrofit) {
        return retrofit.create(PatentService.class);
    }
}
