package br.com.hygorxaraujo.qraniotest.presentation.patents;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.Patent;
import br.com.hygorxaraujo.qraniotest.injection.ActivityContext;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hygor on 12/20/16.
 **/
public class PatentListAdapter
        extends RecyclerView.Adapter<PatentListAdapter.PatentsViewHolder> {

    private List<Patent> patents;
    private PatentsItemClickListener listener;
    private Context context;

    @Inject
    public PatentListAdapter(@ActivityContext Context context) {
        this.context = context;
    }

    public interface PatentsItemClickListener {
        void onPatentItemClick(Patent patent);
    }

    @Override
    public PatentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.patentlist_item_patent, parent, false);
        return new PatentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PatentsViewHolder holder, int position) {
        Patent patent = patents.get(position);
        holder.setPatent(patent);
        holder.tvTitle.setText(context
                .getString(R.string.patentitem_tv_lbl_title,
                        patent.getTitle()));
        holder.tvCategory.setText(context
                .getString(R.string.patentitem_tv_lbl_category,
                        patent.getCategory()));
        holder.tvRefNum.setText(context
                .getString(R.string.patentitem_tv_lbl_reference_number,
                        patent.getReferenceNumber()));
    }

    @Override
    public int getItemCount() {
        return patents != null ? patents.size() : 0;
    }

    public void setPatents(List<Patent> patents) {
        this.patents = patents;
        notifyDataSetChanged();
    }

    public void setListener(PatentListAdapter.PatentsItemClickListener listener) {
        this.listener = listener;
    }

    //region ViewHolder
    class PatentsViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.patentitem_tv_title)
        TextView tvTitle;
        @BindView(R.id.patentitem_tv_category)
        TextView tvCategory;
        @BindView(R.id.patentitem_tv_reference_number)
        TextView tvRefNum;
        private Patent patent;

        public PatentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setPatent(Patent patent) {
            this.patent = patent;
        }

        @OnClick(R.id.patentitem_ll)
        public void onPatentItemClick() {
            listener.onPatentItemClick(patent);
        }
    }
    //endregion
}
