package br.com.hygorxaraujo.qraniotest.presentation.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.injection.component.DaggerActivityComponent;
import br.com.hygorxaraujo.qraniotest.injection.module.ActivityModule;
import br.com.hygorxaraujo.qraniotest.presentation.App;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/12/16.
 **/

public abstract class BaseMvpActivity<V extends MvpView, P extends MvpPresenter<V>>
        extends AppCompatActivity
        implements MvpView {

    protected P presenter;
    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
    }

    private void injectDependencies() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(((App) getApplication()).getApplicationComponent())
                    .build();
        }
        inject(activityComponent);
    }

    protected abstract void inject(ActivityComponent activityComponent);

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onAttach((V) this);
    }

    @Override
    protected void onPause() {
        presenter.onDettach();
        super.onPause();
    }

    public abstract P createPresenter();
}
