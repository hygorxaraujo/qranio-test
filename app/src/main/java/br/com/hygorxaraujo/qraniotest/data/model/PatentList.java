package br.com.hygorxaraujo.qraniotest.data.model;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.List;

/**
 * Created by hygor on 12/20/16.
 **/
@Parcel(Parcel.Serialization.BEAN)
public class PatentList {

    private int count;
    private List<Patent> results;

    @ParcelConstructor
    public PatentList() {
    }

    //region Getters&Setters
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Patent> getResults() {
        return results;
    }

    public void setResults(List<Patent> results) {
        this.results = results;
    }
    //endregion
}
