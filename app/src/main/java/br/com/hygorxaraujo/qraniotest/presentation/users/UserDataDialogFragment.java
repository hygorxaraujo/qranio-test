package br.com.hygorxaraujo.qraniotest.presentation.users;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.parceler.Parcels;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Mask;

/**
 * Created by hygor on 12/14/16.
 **/
public class UserDataDialogFragment
        extends DialogFragment {

    public static UserDataDialogFragment newInstance(User user) {
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.PARCEL_KEY_USER, Parcels.wrap(user));
        UserDataDialogFragment fragment = new UserDataDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        User user = Parcels.unwrap(getArguments()
                .getParcelable(AppConstants.PARCEL_KEY_USER));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialogfragment_userdata, null);
        TextView title = (TextView) view.findViewById(R.id.userdata_tv_title_username);
        title.setText(user.getUserName());
        TextView firstname = (TextView) view.findViewById(R.id.userdata_tv_firstname);
        firstname.setText(getActivity().getString(R.string.userdata_tv_firstname,
                user.getFirstName() + " " + user.getLastName()));
        TextView email = (TextView) view.findViewById(R.id.userdata_tv_email);
        email.setText(getActivity().getString(R.string.userdata_tv_email, user.getEmail()));
        TextView phone = (TextView) view.findViewById(R.id.userdata_tv_phone);
        if (user.getPhone() != null) {
            phone.addTextChangedListener(Mask.insert("(##)#####-####",
                    phone));
            phone.setText(user.getPhone());
            phone.setVisibility(View.VISIBLE);
        } else {
            phone.setVisibility(View.GONE);
        }

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(R.string.userdata_btn_lbl_close, null)
                .create();
    }
}
