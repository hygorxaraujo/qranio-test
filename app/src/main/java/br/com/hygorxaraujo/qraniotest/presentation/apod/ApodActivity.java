package br.com.hygorxaraujo.qraniotest.presentation.apod;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.injection.component.ActivityComponent;
import br.com.hygorxaraujo.qraniotest.presentation.base.BaseMvpActivity;
import br.com.hygorxaraujo.qraniotest.presentation.util.AppConstants;
import br.com.hygorxaraujo.qraniotest.presentation.util.Messenger;
import br.com.hygorxaraujo.qraniotest.presentation.util.ProgressDialogHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ApodActivity
        extends BaseMvpActivity<ApodView, ApodPresenter>
        implements ApodView, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.toolbar_apod)
    Toolbar toolbar;

    @BindView(R.id.apod_iv_img)
    ImageView ivImg;

    @BindView(R.id.apod_tv_title)
    TextView tvTitle;

    @BindView(R.id.apod_tv_date)
    TextView tvDate;

    @BindView(R.id.apod_tv_copyright)
    TextView tvCopyright;

    @BindView(R.id.apod_tv_explanation)
    TextView tvExplanation;

    @Inject
    protected ApodPresenter apodPresenter;

    @Inject
    protected ProgressDialogHelper progressDialogHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apod);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.apod_toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Apod apod = Parcels.unwrap(getIntent().getParcelableExtra(AppConstants.PARCEL_KEY_APOD));
        apodPresenter.onDataAvailable(apod);
    }

    @Override
    protected void onResume() {
        super.onResume();
        apodPresenter.onViewResumed();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public ApodPresenter createPresenter() {
        return apodPresenter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_apod, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.apod_action_date:
                apodPresenter.onMenuDateClick();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //region View
    @Override
    public void setApodData(Apod apod) {
        Picasso.with(this)
                .load(apod.getUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.background_material_red)
                .into(ivImg);
        tvTitle.setText(getString(R.string.apod_tv_title, apod.getTitle()));
        tvDate.setText(getString(R.string.apod_tv_date, apod.getDate()));
        if (apod.getCopyright() != null) {
            tvCopyright.setVisibility(View.VISIBLE);
            tvCopyright.setText(getString(R.string.apod_tv_copyright, apod.getCopyright()));
        } else {
            tvCopyright.setVisibility(View.GONE);
        }
        tvExplanation.setText(getString(R.string.apod_tv_explanation, apod.getExplanation()));
    }

    @Override
    public void showDatePicker() {
        DialogFragment newFragment = DatePickerDialogFragment.newInstance();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void showProgress(int msgResId) {
        progressDialogHelper.show(msgResId);
    }

    @Override
    public void hideProgress() {
        progressDialogHelper.dismiss();
    }

    @Override
    public void showMessage(int msgResId) {
        Messenger.showShort(this, msgResId);
    }
//endregion

    //region Listener
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        apodPresenter.onDateSet(year, month, day);
    }
    //endregion
}
