package br.com.hygorxaraujo.qraniotest.data.remote;

import br.com.hygorxaraujo.qraniotest.data.model.PatentList;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by hygor on 12/20/16.
 **/
public interface PatentService {

    @GET("patents/content")
    Observable<PatentList> getPatents(@Query("api_key") String apiKey,
                                      @Query("limit") int limit,
                                      @Query("query") String query);
}
