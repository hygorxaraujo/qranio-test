package br.com.hygorxaraujo.qraniotest.presentation.patents;

import java.util.List;

import br.com.hygorxaraujo.qraniotest.data.model.Patent;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/20/16.
 **/
public interface PatentListView
        extends MvpView {

    void setUpPatentList(List<Patent> patents);

    void showProgress();

    void hideProgress();

    void showMessage(int msgResId);

    void updatePatentList(List<Patent> patents);

    void showContent();

    void showPatentAbstract(String patentAbstract);
}
