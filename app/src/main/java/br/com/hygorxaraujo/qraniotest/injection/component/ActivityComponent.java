package br.com.hygorxaraujo.qraniotest.injection.component;

import android.content.Context;

import br.com.hygorxaraujo.qraniotest.injection.ActivityContext;
import br.com.hygorxaraujo.qraniotest.injection.ActivityScope;
import br.com.hygorxaraujo.qraniotest.injection.module.ActivityModule;
import br.com.hygorxaraujo.qraniotest.injection.module.WebModule;
import br.com.hygorxaraujo.qraniotest.presentation.apod.ApodActivity;
import br.com.hygorxaraujo.qraniotest.presentation.login.LoginActivity;
import br.com.hygorxaraujo.qraniotest.presentation.patents.PatentListActivity;
import br.com.hygorxaraujo.qraniotest.presentation.register.RegisterActivity;
import br.com.hygorxaraujo.qraniotest.presentation.users.UserListActivity;
import dagger.Component;

/**
 * Created by hygor on 12/13/16.
 **/
@ActivityScope
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, WebModule.class})
public interface ActivityComponent {

    @ActivityContext
    Context context();

    void inject(LoginActivity loginActivity);

    void inject(UserListActivity userListActivity);

    void inject(RegisterActivity registerActivity);

    void inject(ApodActivity apodActivity);

    void inject(PatentListActivity patentListActivity);
}
