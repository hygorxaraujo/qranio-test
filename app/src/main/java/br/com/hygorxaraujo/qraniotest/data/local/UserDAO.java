package br.com.hygorxaraujo.qraniotest.data.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.data.local.UserContract.UserEntry;
import br.com.hygorxaraujo.qraniotest.data.model.User;
import br.com.hygorxaraujo.qraniotest.presentation.util.Logger;

/**
 * Created by hygor on 12/13/16.
 **/
public class UserDAO {

    private QranioTestDbHelper dbHelper;

    @Inject
    public UserDAO(QranioTestDbHelper qranioTestDbHelper) {
        dbHelper = qranioTestDbHelper;
    }

    @Nullable
    public User authUser(String username, String password) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                UserEntry._ID,
                UserEntry.COLUMN_NAME_FIRSTNAME,
                UserEntry.COLUMN_NAME_LASTNAME,
                UserEntry.COLUMN_NAME_EMAIL,
                UserEntry.COLUMN_NAME_USERNAME,
                UserEntry.COLUMN_NAME_PHONE
        };

        String selection = UserEntry.COLUMN_NAME_USERNAME + " = ? AND "
                + UserEntry.COLUMN_NAME_PASSWORD + " = ?";
        String[] selectionArgs = {username, password};

        Cursor cursor = db.query(UserEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null);
        if (cursor.moveToFirst()) {
            return getUser(cursor);
        } else {
            return null;
        }
    }

    @Nullable
    private User getUser(Cursor cursor) {
        try {
            int idIndex = cursor.getColumnIndexOrThrow(UserEntry._ID);
            int firstNameIndex = cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_FIRSTNAME);
            int lastNameIndex = cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_LASTNAME);
            int emailIndex = cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_EMAIL);
            int userNameIndex = cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_USERNAME);
            int phoneIndex = cursor.getColumnIndexOrThrow(UserEntry.COLUMN_NAME_PHONE);
            User user = new User();
            user.setId(cursor.getLong(idIndex));
            user.setFirstName(cursor.getString(firstNameIndex));
            user.setLastName(cursor.getString(lastNameIndex));
            user.setUserName(cursor.getString(userNameIndex));
            user.setEmail(cursor.getString(emailIndex));
            user.setPhone(cursor.getString(phoneIndex));
            return user;
        } catch (Exception e) {
            Logger.e(UserDAO.class.getSimpleName(), "UserCursor:", e);
            return null;
        }
    }

    public long createUser(String firstname, String lastname, String username, String email, String password, String phone) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_FIRSTNAME, firstname);
        values.put(UserEntry.COLUMN_NAME_LASTNAME, lastname);
        values.put(UserEntry.COLUMN_NAME_USERNAME, username);
        values.put(UserEntry.COLUMN_NAME_EMAIL, email);
        values.put(UserEntry.COLUMN_NAME_PASSWORD, password);
        values.put(UserEntry.COLUMN_NAME_PHONE, phone);

        return db.insert(UserEntry.TABLE_NAME, null, values);
    }

    public List<User> getUsers() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<User> users = new ArrayList<>();

        String[] projection = {
                UserEntry._ID,
                UserEntry.COLUMN_NAME_FIRSTNAME,
                UserEntry.COLUMN_NAME_LASTNAME,
                UserEntry.COLUMN_NAME_EMAIL,
                UserEntry.COLUMN_NAME_USERNAME,
                UserEntry.COLUMN_NAME_PHONE
        };

        Cursor cursor = db.query(UserEntry.TABLE_NAME,
                projection, null, null, null, null, null);

        while (cursor.moveToNext()) {
            users.add(getUser(cursor));
        }
        return users;
    }
}
