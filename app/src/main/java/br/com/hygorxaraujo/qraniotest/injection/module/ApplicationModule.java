package br.com.hygorxaraujo.qraniotest.injection.module;

import android.app.Application;
import android.content.Context;

import br.com.hygorxaraujo.qraniotest.injection.ApplicationContext;
import dagger.Module;
import dagger.Provides;

/**
 * Created by hygor on 12/13/16.
 **/
@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }
}
