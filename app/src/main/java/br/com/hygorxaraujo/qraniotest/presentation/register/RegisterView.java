package br.com.hygorxaraujo.qraniotest.presentation.register;

import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpView;

/**
 * Created by hygor on 12/14/16.
 **/
public interface RegisterView
        extends MvpView {

    void showMessage(int msgResId);

    void navigateBackToLogin(String username, String password);
}
