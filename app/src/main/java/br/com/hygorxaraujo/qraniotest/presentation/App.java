package br.com.hygorxaraujo.qraniotest.presentation;

import android.app.Application;

import com.facebook.stetho.Stetho;

import br.com.hygorxaraujo.qraniotest.injection.component.ApplicationComponent;
import br.com.hygorxaraujo.qraniotest.injection.component.DaggerApplicationComponent;
import br.com.hygorxaraujo.qraniotest.injection.module.ApplicationModule;

/**
 * Created by hygor on 12/13/16.
 **/

public class App
        extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        initInjection();
    }

    private void initInjection() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
