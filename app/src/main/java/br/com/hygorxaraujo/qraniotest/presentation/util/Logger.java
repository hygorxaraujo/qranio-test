package br.com.hygorxaraujo.qraniotest.presentation.util;

import android.util.Log;

import br.com.hygorxaraujo.qraniotest.BuildConfig;

/**
 * Created by hygor on 12/18/16.
 **/
public class Logger {

    public static Boolean DEBUG = BuildConfig.DEBUG;
    public static String TAG = "Logger";

    public static String getTag(String type, String place) {
        return TAG + " em " + place + " (" + type + ") Diz: ";
    }

    public static void e(String place, String msg) {
        if (DEBUG)
            Log.e(getTag("Error", place), msg);
    }

    public static void e(String place, String msg, Exception e) {
        if (DEBUG)
            Log.e(getTag("Error", place), msg, e);
    }

    public static void i(String place, String msg) {
        if (DEBUG)
            Log.i(getTag("Info", place), msg);
    }

    public static void d(String place, String msg) {
        if (DEBUG)
            Log.d(getTag("Debug", place), msg);
    }

    public static void v(String place, String msg) {
        if (DEBUG)
            Log.v(getTag("Verbose", place), msg);
    }
}
