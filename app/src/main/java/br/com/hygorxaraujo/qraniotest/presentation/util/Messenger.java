package br.com.hygorxaraujo.qraniotest.presentation.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import br.com.hygorxaraujo.qraniotest.R;

/**
 * Created by hygor on 12/7/16.
 **/
public class Messenger {

    public static void show(Context context, @StringRes int messageResId, int duration) {
        Toast.makeText(context, messageResId, duration).show();
    }

    public static void showShort(Context context, @StringRes int messageResId) {
        show(context, messageResId, Toast.LENGTH_SHORT);
    }

    public static void showLong(Context context, @StringRes int messageResId) {
        show(context, messageResId, Toast.LENGTH_LONG);
    }

    public static void show(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    public static void showShort(Context context, String message) {
        show(context, message, Toast.LENGTH_SHORT);
    }

    public static void showLong(Context context, String message) {
        show(context, message, Toast.LENGTH_LONG);
    }

    public static void showSnackbar(View view, String message) {
        final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.snackbar_action_close,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(10);
        snackbar.show();
    }
}
