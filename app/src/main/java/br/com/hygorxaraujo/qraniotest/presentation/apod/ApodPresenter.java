package br.com.hygorxaraujo.qraniotest.presentation.apod;

import javax.inject.Inject;

import br.com.hygorxaraujo.qraniotest.R;
import br.com.hygorxaraujo.qraniotest.data.DataManager;
import br.com.hygorxaraujo.qraniotest.data.model.Apod;
import br.com.hygorxaraujo.qraniotest.presentation.mvp.MvpPresenter;
import br.com.hygorxaraujo.qraniotest.presentation.util.Logger;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hygor on 12/18/16.
 **/
public class ApodPresenter
        extends MvpPresenter<ApodView> {

    public static final String TAG = ApodPresenter.class.getSimpleName();

    private Apod apod;
    private DataManager dataManager;
    private Subscription subscription;

    @Inject
    public ApodPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    private void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = null;
    }

    public void onDataAvailable(Apod apod) {
        Logger.d(TAG, String.valueOf(apod));
        this.apod = apod;
    }

    public void onViewResumed() {
        if (isViewAttached()) {
            getView().setApodData(apod);
        }
    }

    public void onMenuDateClick() {
        if (isViewAttached()) {
            getView().showDatePicker();
        }
    }

    public void onDateSet(int year, int month, int day) {
        if (isViewAttached()) {
            getView().showProgress(R.string.apod_msg_loading_apod);
            unsubscribe();
            subscription = dataManager.getApodByDate(year, month, day)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Apod>() {
                        @Override
                        public void onCompleted() {
                            Logger.d(TAG, "onCompleted");
                            getView().hideProgress();
                            unsubscribe();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(TAG, e.getMessage());
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().showMessage(R.string.userlist_msg_error_apod);
                            }
                            unsubscribe();
                        }

                        @Override
                        public void onNext(Apod apod) {
                            Logger.d(TAG, "onNext");
                            if (isViewAttached()) {
                                getView().hideProgress();
                                getView().setApodData(apod);
                            }
                        }
                    });
        }
    }
}
